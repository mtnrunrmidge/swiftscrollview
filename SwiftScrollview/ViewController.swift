//
//  ViewController.swift
//  SwiftScrollview
//
//  Created by Margaret Schroeder on 9/8/17.
//  Copyright © 2017 Margaret Schroeder. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        let scrollView = UIScrollView(frame: self.view.bounds)
        scrollView.backgroundColor = UIColor.blue
        self.view.addSubview(scrollView)

        scrollView.contentSize = CGSize(width: self.view.bounds.size.width * 3, height: self.view.bounds.size.height * 3)
        
        //yellow 0,0
        let view = UIView(frame:CGRect(x:0, y:0, width: self.view.bounds.size.height, height: self.view.bounds.size.height))
        view.backgroundColor = UIColor.yellow
        scrollView.addSubview(view)
        
        //green 0,1
        let view1 = UIView(frame:CGRect(x:0, y:self.view.bounds.size.height, width: self.view.bounds.size.width, height: self.view.bounds.size.height))
        view1.backgroundColor = UIColor.green
        scrollView.addSubview(view1)
        
        //purple 0,2
        let view2 = UIView(frame:CGRect(x:0, y:self.view.bounds.size.height * 2, width: self.view.bounds.size.width, height: self.view.bounds.size.height))
        view2.backgroundColor = UIColor.purple
        scrollView.addSubview(view2)
        
        //orange 1,0
        let view3 = UIView(frame:CGRect(x:self.view.bounds.size.width, y:0, width: self.view.bounds.size.width, height: self.view.bounds.size.height))
        view3.backgroundColor = UIColor.orange
        scrollView.addSubview(view3)

        //red 1,1
        let view4 = UIView(frame:CGRect(x:self.view.bounds.size.width, y:self.view.bounds.size.height, width: self.view.bounds.size.width, height: self.view.bounds.size.height))
        view4.backgroundColor = UIColor.red
        scrollView.addSubview(view4)
        
        //black 1,2
        let view5 = UIView(frame:CGRect(x:self.view.bounds.size.width, y:self.view.bounds.size.height * 2, width: self.view.bounds.size.width, height: self.view.bounds.size.height))
        view5.backgroundColor = UIColor.black
        scrollView.addSubview(view5)
        
        //white 2,0
        let view6 = UIView(frame:CGRect(x:self.view.bounds.size.width * 2, y:0, width: self.view.bounds.size.width, height: self.view.bounds.size.height))
        view6.backgroundColor = UIColor.white
        scrollView.addSubview(view6)
        
        //cyan 2,1
        let view7 = UIView(frame:CGRect(x:self.view.bounds.size.width * 2, y:self.view.bounds.size.height, width: self.view.bounds.size.width, height: self.view.bounds.size.height))
        view7.backgroundColor = UIColor.cyan
        scrollView.addSubview(view7)
        
        //leave default for las at blue
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

